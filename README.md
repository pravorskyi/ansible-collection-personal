# Ansible Collection - pravorskyi.main

Installation

$ ansible-galaxy collection install git@gitlab.com:pravorskyi/ansible-collection-personal.git

# Development
Local installation

$ ansible-galaxy collection install . --force

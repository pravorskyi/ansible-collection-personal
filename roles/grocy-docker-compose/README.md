Role Name
=========

grocy-docker-compose

A role to deploy [Grocy](https://grocy.info/) using Docker Compose.

> **_NOTE:_** I would appreciate if this role (or a similar solution) moves into the official repositories of Grocy or linuxserver, or is included as part of a more widely recognized Ansible collection in the future.

Feel free to open an issue or pull request if you have suggestions or fixes.

Requirements
------------

- A system with Docker installed (e.g., `docker` package).
- Docker Compose installed (or the equivalent Compose plugin).
- Ansible 2.9+ (tested with 2.13+).
- Sufficient privileges to manage Docker (e.g., `become: yes` in playbooks).

Role Variables
--------------

Below are the key variables you can override in your inventory or playbooks:

| Variable                | Default         | Description                                                                                           |
|-------------------------|-----------------|-------------------------------------------------------------------------------------------------------|
| `grocy_version`         | `"latest"`      | The Docker image tag of `linuxserver/grocy` to pull and run.                                          |
| `grocy_compose_dir`     | `"/opt/grocy"`  | Directory on the host where the `docker-compose.yml` file and config data are stored.                |
| `grocy_host_port`       | `9283`          | Host port that maps to container port 80 (HTTP).                                                      |
| `grocy_tz`              | `"UTC"`         | Timezone used within the container (for logging, etc.).                                              |
| `grocy_puid`            | `1000`          | PUID (user ID) used by the container, often for file ownership.                                       |
| `grocy_pgid`            | `1000`          | PGID (group ID) used by the container, often for file ownership.                                      |
| `grocy_container_name`  | `"grocy"`       | Docker container name for Grocy.                                                                      |

Dependencies
------------

No external Galaxy roles are required by default.
(Adjust if your environment requires additional roles for Docker or other dependencies.)

Example Playbook
----------------

```yaml
---
- name: Deploy Grocy on servers
  hosts: grocy_server
  become: yes
  roles:
    - role: grocy
      vars:
        grocy_version: "latest"
        grocy_compose_dir: "/opt/grocy"
        grocy_host_port: 9283
        grocy_tz: "Europe/Berlin"
        grocy_puid: 1000
        grocy_pgid: 1000
```

License
-------
GPL 3.0 or later
